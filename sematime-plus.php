<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
Plugin Name:	SematimePlus
Plugin URI:		http://wearearchangel.com/

Description:	A plugin the makes it easy to update content on the website.
Author:			Michael Mwaura
Author URI:		http://michaelmwaura.co.ke
Version:		1.0.0

Text domain:	sematime
License:		GPLv2
*/

/***************************************
Register post types
****************************************/
/* Carousel */
function sematime_register_carousels() {
	$labels = array(
		'name'					=> _x('Carousels', 'sematime'),
		'singular_name'			=> _x('Carousel', 'sematime'),
		'add_new'				=> _x('Add Carousel', 'sematime'),
		'add_new_item'			=> __('Add New Carousel', 'sematime'),
		'edit_item'				=> __('Edit Carousel', 'sematime'),
		'new_item'				=> __('New Carousel', 'sematime'),
		'view_item'				=> __('View Carousel', 'sematime'),
		'search_items'			=> __('Search Carousels', 'sematime'),
		'not_found'				=> __('No Carousels found', 'sematime'),
		'not_found_in_trash'	=> __('No Carousels found in Trash', 'sematime'),
	);
    $args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'rewrite'		=> array( 'slug' => 'carousel' ),
		'supports'		=> array(
			'title',
			'excerpt'
		)
    );
    register_post_type( 'carousel', $args );
}
add_action( 'init', 'sematime_register_carousels' );

/* Departments */
function sematime_register_departments() {
	$labels = array(
		'name'					=> _x('Departments', 'sematime'),
		'singular_name'			=> _x('Department', 'sematime'),
		'add_new'				=> _x('Add Department', 'sematime'),
		'add_new_item'			=> __('Add New Department', 'sematime'),
		'edit_item'				=> __('Edit Department', 'sematime'),
		'new_item'				=> __('New Department', 'sematime'),
		'view_item'				=> __('View Department', 'sematime'),
		'search_items'			=> __('Search Departments', 'sematime'),
		'not_found'				=> __('No Departments found', 'sematime'),
		'not_found_in_trash'	=> __('No Departments found in Trash', 'sematime'),
	);
    $args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'rewrite'		=> array( 'slug' => 'department' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'page-attributes'
		)
    );
    register_post_type( 'department', $args );
}
add_action( 'init', 'sematime_register_departments' );

/* Facilities */
function sematime_register_facilities() {
	$labels = array(
		'name'					=> _x('Facilities', 'sematime'),
		'singular_name'			=> _x('Facility', 'sematime'),
		'add_new'				=> _x('Add Facility', 'sematime'),
		'add_new_item'			=> __('Add New Facility', 'sematime'),
		'edit_item'				=> __('Edit Facility', 'sematime'),
		'new_item'				=> __('New Facility', 'sematime'),
		'view_item'				=> __('View Facility', 'sematime'),
		'search_items'			=> __('Search Facilities', 'sematime'),
		'not_found'				=> __('No Facilities found', 'sematime'),
		'not_found_in_trash'	=> __('No Facilities found in Trash', 'sematime'),
	);
    $args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'rewrite'		=> array( 'slug' => 'facility' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'page-attributes'
		)
    );
    register_post_type( 'facility', $args );
}
add_action( 'init', 'sematime_register_facilities' );

/* Co-curricular */
function sematime_register_co_curricular_activities() {
	$labels = array(
		'name'					=> _x('Co-curriculars', 'sematime'),
		'singular_name'			=> _x('Co-curricular', 'sematime'),
		'add_new'				=> _x('Add Activity', 'sematime'),
		'add_new_item'			=> __('Add New Co-curricular Activity', 'sematime'),
		'edit_item'				=> __('Edit Co-curricular Activity', 'sematime'),
		'new_item'				=> __('New Co-curricular Activity', 'sematime'),
		'view_item'				=> __('View Co-curricular Activity', 'sematime'),
		'search_items'			=> __('Search Co-curricular Activities', 'sematime'),
		'not_found'				=> __('No Co-curricular Activities found', 'sematime'),
		'not_found_in_trash'	=> __('No Co-curricular Activities found in Trash', 'sematime'),
	);
    $args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'rewrite'		=> array( 'slug' => 'co-curricular' ),
		'supports'		=> array(
			'title',
			'thumbnail',
			'excerpt',
			'editor'
		)
    );
    register_post_type( 'co-curricular', $args );
}
add_action( 'init', 'sematime_register_co_curricular_activities' );

/* Staff */
function sematime_register_staff() {
	$labels = array(
		'name'					=> _x('Staff', 'sematime'),
		'singular_name'			=> _x('Staff', 'sematime'),
		'add_new'				=> _x('Add Staff', 'sematime'),
		'add_new_item'			=> __('Add New Staff', 'sematime'),
		'edit_item'				=> __('Edit Staff', 'sematime'),
		'new_item'				=> __('New Staff', 'sematime'),
		'view_item'				=> __('View Staff', 'sematime'),
		'search_items'			=> __('Search Staff', 'sematime'),
		'not_found'				=> __('No Staff found', 'sematime'),
		'not_found_in_trash'	=> __('No Staff found in Trash', 'sematime'),
	);
    $args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'rewrite'		=> array( 'slug' => 'staff' ),
		'supports'		=> array(
			'title',
			'thumbnail',
			'excerpt',
			'editor',
			'custom-fields'
		)
    );
    register_post_type( 'staff', $args );
}
add_action( 'init', 'sematime_register_staff' );

/* Board of Directors */
function sematime_register_boards() {
	$labels = array(
		'name'					=> _x('Boards', 'sematime'),
		'singular_name'			=> _x('Board', 'sematime'),
		'add_new'				=> _x('Add Board', 'sematime'),
		'add_new_item'			=> __('Add New Board', 'sematime'),
		'edit_item'				=> __('Edit Board', 'sematime'),
		'new_item'				=> __('New Board', 'sematime'),
		'view_item'				=> __('View Board', 'sematime'),
		'search_items'			=> __('Search Boards', 'sematime'),
		'not_found'				=> __('No Boards found', 'sematime'),
		'not_found_in_trash'	=> __('No Boards found in Trash', 'sematime'),
	);
    $args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'rewrite'		=> array( 'slug' => 'board' ),
		'supports'		=> array(
			'title',
			'excerpt'
		)
    );
    register_post_type( 'board', $args );
}
add_action( 'init', 'sematime_register_boards' );

/* Achievements */
function sematime_register_achievements() {
	$labels = array(
		'name'					=> _x('Key Achievements', 'sematime'),
		'singular_name'			=> _x('Key Achievement', 'sematime'),
		'add_new'				=> _x('Add Achievement', 'sematime'),
		'add_new_item'			=> __('Add New Achievement', 'sematime'),
		'edit_item'				=> __('Edit Achievement', 'sematime'),
		'new_item'				=> __('New Achievement', 'sematime'),
		'view_item'				=> __('View Achievement', 'sematime'),
		'search_items'			=> __('Search Achievements', 'sematime'),
		'not_found'				=> __('No Achievements found', 'sematime'),
		'not_found_in_trash'	=> __('No Achievements found in Trash', 'sematime'),
	);
    $args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'rewrite'		=> array( 'slug' => 'achievement' ),
		'supports'		=> array(
			'title',
			'excerpt'
		)
    );
    register_post_type( 'achievement', $args );
}
add_action( 'init', 'sematime_register_achievements' );


/***************************************
Register taxonomies
****************************************/
/* Board Members */
function sematime_register_carousel_items_taxonomies() {
	$labels = array(
		'name'				=> _x('Slides', 'sematime' ),
		'singular_name'		=> _x('Slide','sematime' ),
		'search_items'		=> __('Search Slides','sematime' ),
		'all_items'			=> __('All Slides','sematime' ),
		'edit_item'			=> __('Edit Slide','sematime' ),
		'update_item'		=> __('Update Slide','sematime' ),
		'add_new_item'		=> __('Add New Slide', 'sematime'),
		'new_item_name'		=> __('New Slide name', 'sematime'),
		'menu_name'			=> __('Slides', 'sematime'),
	);
	$args = array(
		'hierarchical'		=> true,
		'show_admin_column'	=> true,
		'rewrite'			=> array( 'slug' => 'board-member'),
		'labels'			=> $labels,
	);
	register_taxonomy( 'slide', 'carousel' , $args );
}
add_action( 'init', 'sematime_register_carousel_items_taxonomies');

/* Board Members */
function sematime_register_board_members_taxonomies() {
	$labels = array(
		'name'				=> _x('Board Members', 'sematime' ),
		'singular_name'		=> _x('Board Member','sematime' ),
		'search_items'		=> __('Search Board Members','sematime' ),
		'all_items'			=> __('All Board Members','sematime' ),
		'edit_item'			=> __('Edit Board Member','sematime' ),
		'update_item'		=> __('Update Board Member','sematime' ),
		'add_new_item'		=> __('Add New Board Member', 'sematime'),
		'new_item_name'		=> __('New Board Member name', 'sematime'),
		'menu_name'			=> __('Board Members', 'sematime'),
	);
	$args = array(
		'hierarchical'		=> true,
		'show_admin_column'	=> true,
		'rewrite'			=> array( 'slug' => 'board-member'),
		'labels'			=> $labels,
	);
	register_taxonomy( 'board-member', 'board' , $args );
}
add_action( 'init', 'sematime_register_board_members_taxonomies');

/* Staff Members */
function sematime_register_staff_members_taxonomies() {
	$labels = array(
		'name'				=> _x('Staff Members', 'sematime' ),
		'singular_name'		=> _x('Staff Member','sematime' ),
		'search_items'		=> __('Search Staff Members','sematime' ),
		'all_items'			=> __('All Staff Members','sematime' ),
		'edit_item'			=> __('Edit Staff Member','sematime' ),
		'update_item'		=> __('Update Staff Member','sematime' ),
		'add_new_item'		=> __('Add New Staff Member', 'sematime'),
		'new_item_name'		=> __('New Staff Member name', 'sematime'),
		'menu_name'			=> __('Staff Members', 'sematime'),
	);
	$args = array(
		'hierarchical'		=> true,
		'show_admin_column'	=> true,
		'rewrite'			=> array( 'slug' => 'staff-member'),
		'labels'			=> $labels,
	);
	register_taxonomy( 'staff-member', 'staff' , $args );
}
add_action( 'init', 'sematime_register_staff_members_taxonomies');
?>